//Em JS as funções são tratadas como um tipo de dados

//criar função de forma literal
function fun1(){} //a função que não retorna nenhum dado, retorna undefined

//É possível armazenar uma função em uma variável
const fun2 = function(){} //assim, é possível invocar a função com fun2(). Também é possível passar como parâmetro como fun1(fun2)

//É possível armazenar uma função em um array
const array = [function(a,b){return a+b}, fun1, fun2] //execução da função: array[0](1, 2)

//Também é possível armazenar uma função dentro de um atributo de um objeto
const obj = {}
obj.falar = function(){return 'opa'} //como executar: obj.falar()

//Podemos passar uma função como parâmetro de outra função
function run(fun1){fun1()}

//Também é possível retornar/conter uma função
function soma(a,b){
    return function(c){
        console.log(a+b+c)
    }
} 
/**
 * exemplo de execução: soma(2,3)(4)
 * ou ainda:
 * const cincoMais = soma(2,3)
 * cincoMais(4) //visto que soma(a,b) retorna uma função, passa-se o parâmetro para obter o resultado
 */




