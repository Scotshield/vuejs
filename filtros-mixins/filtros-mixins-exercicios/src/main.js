import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

//declarando um mixin global, que será aplicado em todos os componentes
Vue.mixin({
	created(){
		console.log('Created - Mixin Global')
	}
})

//declarando um filtro globalmente
Vue.filter('inverter', function(valor){
	return valor.split('').reverse().join('')
})

new Vue({
	render: h => h(App)
}).$mount('#app')
