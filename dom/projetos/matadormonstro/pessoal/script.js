new Vue({
    el: '#app',
    data: {
        jogador: {
            vida: 100,
            log: [],
            vivo: true
        },
        monstro: {
            vida: 100,
            log: [],
            vivo: true
        },
        iniciouJogo: false,
        endGame: false
    },
    computed: {
        buttonClass(){
            return ['noborder','button']
        }
    },
    watch:{
        'jogador.vida'(novo, velho){
            if(novo < 0){
                this.jogador.vida = 0;
                this.endGame = true
                this.jogador.vivo = false;
            }
        },
        'monstro.vida'(novo, velho){
            if(novo < 0){
                this.monstro.vida = 0;
                this.endGame = true
                this.mostro.vivo = false;
            }
        }

    },
    methods: {
        iniciarJogo(){
            this.iniciouJogo = true
        },
        sair(){
            this.iniciouJogo = false
            this.jogador.vida = 100
            this.jogador.log = []
            this.monstro.vida = 100
            this.monstro.log = []
            this.endGame = false
        },
        atacar(){
            this.monstroAtaque()
            this.jogadorAtaque(1,6)
        },
        ataqueEspecial(){
            this.monstroAtaque()
            this.jogadorAtaque(3,9)
        },
        curar(){
            this.monstroAtaque()
            let mana = this.intRand(4,10)
            this.jogador.vida += mana
            this.jogador.log.push('VOCÊ USOU MANA E RECUPEROU '+mana+ ' DE VIDA')
        },
        jogadorAtaque(min, max){
            let ataque = this.intRand(min,max)
            this.monstro.vida -= ataque //ataque do jogador
            this.jogador.log.push('VOCÊ ATACOU O MONSTRO COM FORÇA DE '+ataque)
        },
        monstroAtaque(){
            let ataque = this.intRand(2,8)
            this.jogador.vida -= ataque
            this.monstro.log.push('MONSTRO ATACOU COM FORÇA DE '+ataque)
        },
        intRand(min, max){
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min) + min); 
        }
    }
})