import Vue from 'vue'
import App from './App.vue'
import Contadores from './Contadores.vue'

Vue.config.productionTip = false
Vue.component('app-contadores', Contadores)

new Vue({
  el:'#app',
  render: h => h(App),
})
